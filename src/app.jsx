import React from 'react';
import styled, {createGlobalStyle} from 'styled-components';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import HomePage from './components/homePage';
import CreatePage from './components/createPage';
import ListPage from './components/listPage';
import TrainingPage from "./components/training";

const MainWrap = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
`;

const GlobalStyle = createGlobalStyle`
  body {
    background-color: #cbcbcb;
    margin: 0;
    font-family: sans-serif;
  }
  
  *,
  *:before,
  *:after {
    box-sizing: border-box;
  }
`;

const App = () => {
  return (
    <MainWrap>
      <GlobalStyle/>
      <Router>
        <div>
          <Switch>
            <Route exact path="/">
              <HomePage />
            </Route>
            <Route path="/training">
              <TrainingPage />
            </Route>
            <Route path="/create">
              <CreatePage />
            </Route>
            <Route path="/list">
              <ListPage />
            </Route>
          </Switch>
        </div>
      </Router>
    </MainWrap>
  );
};

export default App;
