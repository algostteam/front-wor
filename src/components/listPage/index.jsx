import React from 'react';
import styled from 'styled-components'

const WrapListPage = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
`;

const ListPage = () => {
  return (
    <WrapListPage>
      ListPage
    </WrapListPage>
  );
};

export default ListPage;
