import React from 'react';
import styled from 'styled-components';

const Wrap = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: ${({justifyContent}) => justifyContent};
  align-items: ${({alignItems}) => alignItems};

  padding: 20px;
  border: 1px solid #a0a0a0;
  border-radius: 5px;
  background-color: #ffffff;
  height: 100%;
  width: 100% - 40px;
  box-shadow: 0 0 15px rgba(0,0,0,0.4);
`;

const Panel = ({ justifyContent = 'space-around', alignItems = 'center', children } ) => {
  return (
    <Wrap justifyContent={justifyContent} alignItems={alignItems}>
      {children}
    </Wrap>
  );
};

export default Panel;
