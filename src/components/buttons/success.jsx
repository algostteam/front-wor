import React from 'react';
import styled from 'styled-components';

const WrapButton = styled.div`
  width: ${({width}) => width}px;
  height: ${({height}) => height}px;
  cursor: pointer;
`;

const Button = styled.button`
  border-radius: 5px;
  width: 100%;
  height: 100%;
  font-size: ${({fontSize}) => fontSize}px;
  color: #ffffff;
  background-color: #5cb85c;
  border: 1px solid #4cae4c;
  cursor: pointer;
`;

const SuccessButton = (props) => {
  const { width, height, fontSize, children, onClick } = props;

  return (
    <WrapButton width={width} height={height}>
      <Button fontSize={fontSize} onClick={onClick}>
        {children}
      </Button>
    </WrapButton>
  );
};

export default SuccessButton;
