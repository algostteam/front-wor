import React from 'react';
import styled from 'styled-components'

const WrapTrainingPage = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
`;

const TrainingPage = () => {
  return (
    <WrapTrainingPage>
      TrainingPage
    </WrapTrainingPage>
  );
};

export default TrainingPage;
