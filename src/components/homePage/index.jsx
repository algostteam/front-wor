import React from 'react';
import styled from 'styled-components'
import {
  useHistory,
} from "react-router-dom";
import SuccessButton from '../buttons/success'
import Panel from '../panel'

const WrapHomePage = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
`;

const WrapPanel = styled.div`
  width: 900px;
  height: 700px;
`;

const HomePage = () => {
  const history = useHistory();

  return (
      <WrapHomePage>
        <WrapPanel>
          <Panel>
            <SuccessButton width={700} height={150} fontSize={60} onClick={() => history.push('/training')}>
              Тренажер
            </SuccessButton>
            <SuccessButton width={700} height={150} fontSize={60} onClick={() => history.push('/create')}>
              Добавить слово
            </SuccessButton>
            <SuccessButton width={700} height={150} fontSize={60} onClick={() => history.push('/list')}>
              Список слов
            </SuccessButton>
          </Panel>
        </WrapPanel>
      </WrapHomePage>
  );
};

export default HomePage;
