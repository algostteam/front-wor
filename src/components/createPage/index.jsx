import React, { useState, useRef, useEffect } from 'react';
import styled from 'styled-components';
import Panel from '../panel';
import { observer } from "mobx-react";

const WrapCreatePage = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
`;

const WrapPanel = styled.div`
  width: 900px;
  height: 700px;
`;

const LabelWrap = styled.div`
  font-size: 30px;
  margin: 20px 0;
  min-height: 34px;
`;

const TextArea = styled.textarea`
  font-size: 30px;
  width: 100%;
  resize: vertical;
`;

const CreatePage = observer(() => {
  const [ phase, setPhase ] = useState(0);
  const [ wordInfo, setWordInfo ] = useState({});
  const textAreaOriginalPhrase = useRef();
  const textAreaTranslatePhrase = useRef();

  useEffect(() => {
    if (phase === 0) {
      textAreaOriginalPhrase.current.focus();
    }

    if (phase === 1) {
      textAreaTranslatePhrase.current.focus();
    }
  }, [phase]);


  const renderLabel = (label) => {
    return (
      <>
        <LabelWrap>
          {label}
        </LabelWrap>
      </>
    )
  };

  const renderTextarea = ({ ref, onKeyDown, placeholder }) => {
    return (
      <>
        <TextArea ref={ref} onKeyDown={onKeyDown} placeholder={placeholder} />
      </>
    );
  };

  const renderFirstLabel = () => {
    const phaseContents = {
      0: renderLabel('Введите слово вместе с контекстом:'),
    };


    return phaseContents[phase] || renderLabel('');
  };

  const renderOriginalPhrase = () => {
    const onEnterFirstPhase = (event) => {
      if (event.keyCode !== 13) {
        return;
      }

      const value = event.target.value;

      setWordInfo(prevWordInfo => {
        prevWordInfo.originalPhrase = value;

        return prevWordInfo;
      });
      setPhase(prevPhase => prevPhase + 1);
    };

    const phaseContents = {
      0: renderTextarea({ ref: textAreaOriginalPhrase, onKeyDown: onEnterFirstPhase, placeholder: 'Фраза...', }),
      1: renderLabel(wordInfo.originalPhrase),
      2: renderLabel(wordInfo.originalPhrase),
    };


    return phaseContents[phase] || renderLabel('');

  };

  const renderSecondLabel = () => {
    const phaseContents = {
      1: (
        <>
          <LabelWrap>
            Введите перевод фразы:
          </LabelWrap>
        </>
      )
    };

    return phaseContents[phase] || renderLabel('');
  };

  const renderTranslatePhrase = () => {
    const onEnterSecondPhase = (event) => {
      if (event.keyCode !== 13) {
        return;
      }

      const value = event.target.value;

      setWordInfo(prevWordInfo => {
        prevWordInfo.translatePhrase = value;

        return prevWordInfo;
      });
      setPhase(prevPhase => prevPhase + 1);
    };

    const phaseContents = {
      1: renderTextarea({ ref: textAreaTranslatePhrase, onKeyDown: onEnterSecondPhase, placeholder: 'Перевод фразы...', }),
      2: renderLabel(wordInfo.translatePhrase),
    };


    return phaseContents[phase] || renderLabel('');
  };

  return (
    <WrapCreatePage>
      <WrapPanel>
        <Panel justifyContent={'flex-start'} alignItems={'flex-start'}>
          {renderFirstLabel()}
          {renderOriginalPhrase()}
          {renderSecondLabel()}
          {renderTranslatePhrase()}
        </Panel>
      </WrapPanel>
    </WrapCreatePage>
  );
});

export default CreatePage;
